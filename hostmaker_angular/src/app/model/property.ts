export class Address {
    constructor(
        public line1: string,
        public line2: string,
        public line3: string,
        public line4: string,
        public postCode: string,
        public city: string,
        public country: string,
    ) { }
}

export class Property {
    constructor(
        public owner: string,
        public airbnbId: number,
        public numberOfBedrooms: number,
        public numberOfBathrooms: number,
        public incomeGenerated: number,
        public address: Address,
    ) { }
}