import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  public isAdd: boolean = false;
  constructor() { }
  ngOnInit() {
  }

  addClicked() {
    this.isAdd = true;
  }

  searchClicked() {
    this.isAdd = false;
  }
}
