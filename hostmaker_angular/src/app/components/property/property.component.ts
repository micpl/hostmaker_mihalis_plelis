import { Component, OnInit, Input } from '@angular/core';
import 'rxjs/add/operator/map';
import { Property, Address } from '../../model/property';
import { ApiService } from '../../services/api.service';
import { OnChanges } from '@angular/core/src/metadata/lifecycle_hooks';


@Component({
  selector: 'app-property',
  templateUrl: './property.component.html',
  styleUrls: ['./property.component.css']
})

export class PropertyComponent implements OnInit, OnChanges {
  @Input() public edit: boolean = false;
  @Input() public showVersions: boolean = false;
  @Input() public property: Property = null;
  private propertyVersions: Property[] = [];
  private hideForm: boolean = false;

  constructor(private api: ApiService) { }

  ngOnInit() {
  }

  ngOnChanges() {
    this.hideForm = false;
    if (!this.edit) {
      this.property = new Property("", 0, 0, 0, 0, new Address("", "", "", "", "", "", ""));
    }
  }

  showAll() {
    this.propertyVersions = [];
    this.api.getPropertyVersions(this.property.airbnbId).subscribe(
      response => {
        this.showVersions = true;
        for (let property in response) {
          this.propertyVersions.push(response[property]);
        }
      },
      err => {
        alert(JSON.stringify(err));
      },
    )
  }

  delete() {
    this.showVersions = false;
    this.api.deleteProperty(this.property.airbnbId).subscribe(
      response => {
        if (response==undefined) {
          alert('Deleted Successfully!')
        } else {
          alert(JSON.stringify(response));
        }
        this.edit = false;
        this.hideForm = true;
      },
      err => {
        alert(JSON.stringify(err));
      },
    )
  }

  submit() {
    this.showVersions = false;
    if (this.edit) {
      this.api.updateProperty(this.property).subscribe(
        response => {
          alert(JSON.stringify(response));
        },
        err => {
          alert(JSON.stringify(err));
        },
      )
    } else {
      this.api.createProperty(this.property).subscribe(
        response => {
          alert(JSON.stringify(response));
        },
        err => {
          alert(JSON.stringify(err));
        },
      )
    }
  }
}
