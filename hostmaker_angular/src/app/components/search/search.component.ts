import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { Property } from '../../model/property';
import { OnChanges, AfterViewChecked } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
  providers: [ApiService],
})
export class SearchComponent implements OnInit, OnChanges {
  @Input() public add: boolean = false;
  private airbnbid: number;
  private propertyFound: Property = null;

  constructor(public api: ApiService) { }

  ngOnInit() {
    this.propertyFound = null;
  }

  ngOnChanges(changes: SimpleChanges) {
    if ('add' in changes) {
      let change = changes['add'];
      let curVal = change.currentValue;
      let prevVal = change.previousValue;

      if (curVal) {
        this.propertyFound = null;
      }
    }
  }

  onSubmit() {
    this.propertyFound = null;
    if (!this.airbnbid) {
      return alert('Please use a valid AirBnB ID');
    }
    this.api.getProperty(this.airbnbid).subscribe(
      response => {
        if ('message' in response) {
          alert('Server returned error: ' + response['message']);
        } else {
          this.propertyFound = response;
        }
      },
      err => {
        this.propertyFound = null;
        alert("Failed to communicate with the server: " + err);
      }
    )
  }

}
