import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import { Property } from '../model/property';

@Injectable()
export class ApiService {

  private propertyUrl = "http://localhost:8088/hostmaker/property";

  constructor(public http: Http) { }

  createProperty(body: Object): Observable<Property> {
    return this.http.post(`${this.propertyUrl}/`, body) // post request to create property
      .map((res: Response) => res.json()) // calling .json() on the response to return data
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  getProperty(id: number): Observable<Property> {
    return this.http.get(`${this.propertyUrl}/${id}`)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  getPropertyVersions(id: number): Observable<Property> {
    return this.http.get(`${this.propertyUrl}/${id}/all`)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  deleteProperty(id: number): Observable<Property> {
    return this.http.delete(`${this.propertyUrl}/${id}`)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  updateProperty(body: Object): Observable<Property> {
    return this.http.put(`${this.propertyUrl}/`, body)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
}
