# Hostmaker AirBnB Management application
### Author : Mihalis Plelis
### Version : 1.0.0

This Node JS application manages AirBnB properties and keeps a history of their data.
The front-end of the application is made with Angular 5.

### Functionality Description

* Properties can be inserted into the database. Below, the request body is shown as well as the resources of the application.  
		
* The API exposes operations for  
	- Adds a property.  
	- Returns the latest property or a list of all the update versions of this property.  
	- Updates a property.  
	- Deletes a property.  

### Getting Started with the Node JS application
* Make sure you have installed Node and NPM from https://nodejs.org/en/ before moving forward.  
* Also Docker has to be installed in order to keep the PostgreSQL containers for the development and testing environments.  
  Otherwise, the databases will have to be created manually with the properties which can be found in */server/config/config.json* file.  

### Running Video Rental Store Application locally in IDE

1. Navigate in the project's folder, and type the following command which will install all dependencies and modules.  
    *npm install*  

2. Now navigate in the */stubs* directory of the project, and type the command *docker compose up -d*.  
   This command will download, build and start the database containers.   
   If docker is not available, then those database instances will have to be created manually using Postgres.

3. Afterwards install *sequelize* globally which is and ORM framework with the command *npm install -g sequelize*, and execute   
   the following command in order to make the database migrations from the root project directory.  

   For the local database:   
   *sequelize db:migrate --url 'postgresql://user:password@192.168.99.100:5432/hostmaker'*  

   For the testing database:  
   *sequelize db:migrate --url 'postgresql://user:password@192.168.99.100:5432/test'*  

   You can undo all those migrations by executing the command *sequelize db:migrate* followed by the database URL.  

4. Now, you should be able to run the application with the command, *npm run dev* and also unit test it with the command, *npm test*.  
   The server is running on port 8088 on localhost by default. Example URL : *http://localhost:8088/hostmaker/property/*
   This application was tested on Windows, so please make sure that you are using the correct URL on Mac/Linux and also make sure to  
   use the correct path for the docker containers as well in order to use the database. In case the parameters are different,  
   please make sure to update them in the *config.json* file.

5. Congratulations! Now you should be able to send HTTP requests to the API.

### Exposed endpoints

*	The following resources have been registered which can be accessed on the URL *http://localhost:8088/hostmaker/property/*:

	- POST    http://localhost:8088/hostmaker/property/ -> (creates a new property)  
	- GET     http://localhost:8088/hostmaker/property/{airbnb_id_of_the_property} -> (returns the property with this specific AirBnB ID)  
	- GET     http://localhost:8088/hostmaker/property/{airbnb_id_of_the_property}/all -> (returns all of the property versions)  
	- PUT     http://localhost:8088/hostmaker/property/ -> (updates the property)  
	- DELETE  http://localhost:8088/hostmaker/property/{airbnb_id_of_the_property} -> (deletes the property with the specified AirBnB ID)  
    
    Those resources can be accessed by the application Postman. It can be downloaded at https://www.getpostman.com/
    - A Postman collection has been attached to this project. It can be found under the path */scripts/hostmaker.postman_collection.json*
    - After Postman has been installed, please select the option to import a collection.
    - Then you can navigate to the path */scripts/hostmaker.postman_collection.json* and import the file *hostmaker.postman_collection.json*
    - Now you should be able to fire requests against the service which is running locally.  

*   The application can be accessed from the Angular GUI as well. Instructions for running it, are provided in the next section.


### How to install and run the Angular application

*   Make sure you have installed Node before installing Angular. Also please install Angular CLI with the command "*npm install -g @angular/cli*"  
*   Now navigate in the root project directory, and execute the command *npm install* to install all dependencies and modules.  
*   You should now be able to run the project, with the command "*ng serve*".  
*   Angular is running on *http://localhost:4200/* by default. You should be able to access it from your browser.  
*   You can search for a property using it's AirBnB ID in the database.  
*   You can also add a new property with the *Add* button on the top right corner.  
*   When you have successfully found a property using the search button, you can either Update, view all of it's versions at the bottom of the page  
    or delete it.
*   Now you can have fun!

### How the body of the resources should be

Inside the Postman collection, all the requests with their bodies can be found.  
The GET and DELETE requests are using the AirBnB ID in the URL while the POST and PUT requests are using a body which is actually the same.  

* Example  
```json
{    
	"owner": "carlos",
	"address": {
	    "line1": "Flat 5",
	    "line4": "7 Westbourne Terrace",
	    "postCode": "W2 3UL",
	    "city": "London",
	    "country": "U.K."
	},
	"airbnbId": 2354700,
	"numberOfBedrooms": 1,
	"numberOfBathrooms": 1,
	"incomeGenerated": 2000.34
}
```

### Notes

- 	When the requests contain malformed JSON input or wrong values, the relevant errors will be returned.  
-   Only some basic test cases were added since the time was limited. Extensive test coverage for both the back and front-end could have been used.
- 	Server side rendering was used, since this allows the user to see the interface faster than with server side rendering.  
    Also the server is not getting too overloaded when there are many requests at the same time.  
    In both cases, there are advantages and disadvantages.
-   The AirBnB ID is validated on the server, since the user's IP address might have been blocked for some reason by AirBnB.
    In order to avoid server blocking by AirBnB, a request with only one header is sent. A proxy could have been used as well.
