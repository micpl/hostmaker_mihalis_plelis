process.env.NODE_ENV = 'test';
process.env.PORT = '5000';

let chai = require('chai');
let chaiHttp = require('chai-http');
let www = require('../bin/www');
let app = require('../app');
let should = chai.should();

chai.use(chaiHttp);

describe('Properties', () => {
    // Clear the database before starting
    before((done) => {
        let airbnbId = 123;
        chai.request(app)
            .delete(`/hostmaker/property/${airbnbId}`)
            .end((err, res) => {
                done();
            });
    });

    describe('/GET all properties', () => {
        it('it should GET all the properties', (done) => {
            chai.request(app)
                .get('/hostmaker/property')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.be.eql(0);
                    done();
                });
        });
    });

    describe('/POST property', () => {
        it('it should POST a property and return 200 status code', (done) => {
            let property = {
                owner: "dummy",
                address: {
                    "line1": "dummy line 1",
                    "line4": "dummy line 4",
                    "postCode": "DUMMY post code",
                    "city": "DUMMY CITY",
                    "country": "DUMMY country"
                },
                airbnbId: 123,
                numberOfBedrooms: 11,
                numberOfBathrooms: 11,
                incomeGenerated: 111.11
            }
            chai.request(app)
                .post('/hostmaker/property')
                .send(property)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('message').eql('The property with AirBnB ID 123 was successfully created');
                    done();
                });
        });
    });

    describe('/GET a single property', () => {
        it('it should "Property not Found" for a non existent property', (done) => {
            chai.request(app)
                .get('/hostmaker/property/432')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.have.property('message').eql('Property Not Found');
                    done();
                });
        });

        it('it should return 200 status code with the property body', (done) => {
            chai.request(app)
                .get('/hostmaker/property/123')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('owner').eql('dummy');
                    res.body.should.have.property('airbnbId');
                    res.body.should.have.property('address');
                    res.body.should.have.property('numberOfBedrooms');
                    done();
                });
        });
    });

    describe('/PUT a property', () => {
        let property = {
            owner: "dummy",
            address: {
                "line1": "dummy line 13",
                "line4": "dummy line 42",
                "postCode": "DUMMY post code1",
                "city": "DUMMY CITY3",
                "country": "DUMMY country3"
            },
            airbnbId: 123,
            numberOfBedrooms: 11,
            numberOfBathrooms: 11,
            incomeGenerated: 111.11
        }

        it('it should update a property', (done) => {
            chai.request(app)
                .put('/hostmaker/property')
                .send(property)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('message').eql('The property with AirBnB ID 123 was successfully updated');
                    done();
                });
        });

        it('it should return "Property Not Found" when trying to update not existing property', (done) => {
            property.airbnbId = 12356;
            chai.request(app)
                .put('/hostmaker/property')
                .send(property)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.have.property('message').eql('Property Not Found');
                    done();
                });
        });
    });

    describe('/DELETE a property', () => {
        let airbnbId = 123;
        it('it should delete an existing property', (done) => {
            chai.request(app)
                .delete(`/hostmaker/property/${airbnbId}`)
                .end((err, res) => {
                    res.should.have.status(204);
                    res.body.should.be.empty;
                    done();
                });
        });

        it('it should "Property Not Found" on deleting non existing property', (done) => {
            chai.request(app)
                .delete(`/hostmaker/property/${airbnbId}`)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.have.property('message').eql('Property Not Found');
                    done();
                });
        });
    });
});