const express = require('express');
const logger = require('morgan');
const bodyParser = require('body-parser');

// Set up the express app
const app = express();

const propertyRoutes = require('./server/routes/property');

// Log requests to the console.
if (process.env.NODE_ENV !== 'test') {
  app.use(logger('dev'));
}

// Parse incoming requests data using body parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Add Access Control headers
app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  res.setHeader('Access-Control-Allow-Credentials', false);
  next();
});

// Require our routes into the application.
app.use('/hostmaker/property', propertyRoutes);

// Default route that sends back a message in JSON format if the other routes aren't called.
app.get('*', (req, res) => res.status(200).send({
  message: 'Welcome to the Hostmaker\'s API!',
}));

module.exports = app;