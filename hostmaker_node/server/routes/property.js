const express = require('express');
const router = express.Router();
const propertyController = require('../controllers').property;

router.post('/', propertyController.create);
router.get('/:airbnbId', propertyController.retrieveLatest);
router.get('/:airbnbId/all', propertyController.retrieve);
router.get('/', propertyController.list);

router.put('/', propertyController.update);

router.delete('/:airbnbId', propertyController.remove);

module.exports = router;