'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Properties', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      owner: {
        type: Sequelize.TEXT,
        allowNull: false,
      },
      airbnbId: {
        type: Sequelize.BIGINT,
        allowNull: false,
      },
      numberOfBedrooms: {
        type: Sequelize.SMALLINT,
        allowNull: false,
        validate: { min: 0 },
      },
      numberOfBathrooms: {
        type: Sequelize.SMALLINT,
        allowNull: false,
        validate: { min: 1 },
      },
      incomeGenerated: {
        type: Sequelize.DECIMAL(15,2),
        allowNull: false,
        validate: { min: 0.01 },
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false,
      },
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Properties');
  }
};