'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Addresses', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      line1: {
        type: Sequelize.TEXT,
        allowNull: false,
      },
      line2: {
        type: Sequelize.TEXT,
        allowNull: true,
        defaultValue: null,
      },
      line3: {
        type: Sequelize.TEXT,
        allowNull: true,
        defaultValue: null,
      },
      line4: {
        type: Sequelize.TEXT,
        allowNull: false,
      },
      postCode: {
        type: Sequelize.TEXT,
        allowNull: false,
      },
      city: {
        type: Sequelize.TEXT,
        allowNull: false,
      },
      country: {
        type: Sequelize.TEXT,
        allowNull: false,
      },
      propertyId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        unique: true,
        onDelete: 'CASCADE',
        references: {
          model: 'Properties',
          key: 'id',
          as: 'propertyId',
        },
      },
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Addresses');
  }
};