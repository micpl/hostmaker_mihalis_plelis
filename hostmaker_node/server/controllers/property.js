const Property = require('../models').Property;
const Address = require('../models').Address;
var request = require('request');

const excludedAttributes = ['createdAt', 'id', 'propertyId'];

const isPropertyValid = function (property) {
  console.log(property.owner.length)
  console.log(property.numberOfBedrooms)
  if (
    property.owner.length < 1 || property.numberOfBedrooms === undefined ||
    property.airbnbId < 1 || property.airbnbId === undefined ||
    property.numberOfBedrooms < 0 || property.numberOfBedrooms === undefined ||
    property.numberOfBathrooms < 1 || property.numberOfBathrooms === undefined ||
    property.incomeGenerated < 0.01 || property.incomeGenerated === undefined ||
    property.address.line1.length < 1 || property.address.line1.length === undefined ||
    property.address.line4.length < 1 ||  property.address.line4.length === undefined ||
    property.address.postCode.length < 1 ||property.address.postCode.length === undefined ||
    property.address.city.length < 1 || property.address.city.length === undefined ||
    property.address.country.length < 1 || property.address.country.length === undefined
  ) {
    return false;
  }
  return true;
}

module.exports = {
  create(req, res) {
    if (!isPropertyValid(req.body)) {
      return res.status(200).send({ message: 'Please fill in all the required fields correctly.' });
    }
    var host = 'https://www.airbnb.co.uk/rooms/' + req.body.airbnbId;

    var requestOptions = {
      url: host,
      method: 'GET',
      // Use only the 'accept' header in our request, to avoid getting blocked.
      headers: {
        'accept': '*/*',
      },
      followRedirect: false,
    };

    request(requestOptions, function (err, response, body) {
      if (err) {
        return res.status(400).send(err);
      }
      // check if the AirBnB ID is valid
      if (response.statusCode !== 200 && process.env.NODE_ENV !== 'test') {
        return res.status(200).send({ message: 'This is an invalid AirBnB ID.' });
      } else {
        return Property
          .findOne({
            where: {
              airbnbId: req.body.airbnbId
            },
          })
          .then((property) => {
            if (property) {
              return res.status(200).send({ error: 'A property with this AirBnB ID already exists!' });
            }
            return Property
              .create({
                owner: req.body.owner,
                airbnbId: req.body.airbnbId,
                numberOfBedrooms: req.body.numberOfBedrooms,
                numberOfBathrooms: req.body.numberOfBathrooms,
                incomeGenerated: req.body.incomeGenerated,
              })
              .then(property => {
                Address
                  .create({
                    line1: req.body.address.line1,
                    line2: req.body.address.line2,
                    line3: req.body.address.line3,
                    line4: req.body.address.line4,
                    postCode: req.body.address.postCode,
                    city: req.body.address.city,
                    country: req.body.address.country,
                    propertyId: property.id,
                  })
                  .then(() => {
                    return res.status(200).send({ message: `The property with AirBnB ID ${req.body.airbnbId} was successfully created` })
                  })
              })
          })
          .catch(error => {
            return res.status(400).send(error);
          });
      }
    });
  },

  retrieveLatest(req, res) {
    return Property
      .findOne({
        where: {
          airbnbId: req.params.airbnbId
        },
        include: [{
          model: Address,
          as: 'address',
          attributes: { exclude: excludedAttributes }
        }],
        attributes: { exclude: excludedAttributes },
        order: [['createdAt', 'DESC']],
      })
      .then(property => {
        if (!property) {
          return res.status(200).send({
            message: 'Property Not Found',
          });
        }
        return res.status(200).send(property);
      })
      .catch(error => {
        return res.status(400).send(error);
      });
  },

  retrieve(req, res) {
    return Property
      .findAll({
        where: {
          airbnbId: req.params.airbnbId
        },
        include: [{
          model: Address,
          as: 'address',
          attributes: { exclude: excludedAttributes }
        }],
        attributes: { exclude: excludedAttributes },
        order: [['createdAt', 'ASC']],
      })
      .then(properties => {
        return res.status(200).send(properties);
      })
      .catch(error => {
        return res.status(400).send(error);
      });
  },

  list(req, res) {
    return Property
      .findAll({
        include: [{
          model: Address,
          as: 'address',
          attributes: { exclude: excludedAttributes }
        }],
        attributes: { exclude: excludedAttributes }
      })
      .then(properties => {
        return res.status(200).send(properties);
      })
      .catch(error => {
        return res.status(400).send(error);
      });
  },

  update(req, res) {
    if (!isPropertyValid(req.body)) {
      return res.status(200).send({ message: 'Please fill in all the required fields correctly.' });
    }
    return Property
      .findOne({
        where: {
          airbnbId: req.body.airbnbId
        },
        include: [{
          model: Address,
          as: 'address',
        }],
        order: [['createdAt', 'DESC']],
      })
      .then(property => {
        if (!property) {
          return res.status(200).send({
            message: 'Property Not Found',
          });
        }
        var retrievedProperty = req.body;
        var updatedProperty = {
          owner: property.dataValues.owner,
          address: {
            line1: property.dataValues.address.dataValues.line1,
            line2: property.dataValues.address.dataValues.line2,
            line3: property.dataValues.address.dataValues.line3,
            line4: property.dataValues.address.dataValues.line4,
            postCode: property.dataValues.address.dataValues.postCode,
            city: property.dataValues.address.dataValues.city,
            country: property.dataValues.address.dataValues.country,
          },
          airbnbId: property.dataValues.airbnbId,
          numberOfBedrooms: property.dataValues.numberOfBedrooms,
          numberOfBathrooms: property.dataValues.numberOfBathrooms,
          incomeGenerated: property.dataValues.incomeGenerated,
        };
        if (retrievedProperty.address.line2 === undefined) {
          req.body.address.line2 = null;
        }
        if (retrievedProperty.address.line3 === undefined) {
          req.body.address.line3 = null;
        }

        if (retrievedProperty.owner == updatedProperty.owner &&
          retrievedProperty.address.line1 == updatedProperty.address.line1 &&
          retrievedProperty.address.line2 == updatedProperty.address.line2 &&
          retrievedProperty.address.line3 == updatedProperty.address.line3 &&
          retrievedProperty.address.line4 == updatedProperty.address.line4 &&
          retrievedProperty.address.postCode == updatedProperty.address.postCode &&
          retrievedProperty.address.city == updatedProperty.address.city &&
          retrievedProperty.address.country == updatedProperty.address.country &&
          retrievedProperty.airbnbId == updatedProperty.airbnbId &&
          retrievedProperty.numberOfBedrooms == updatedProperty.numberOfBedrooms &&
          retrievedProperty.numberOfBathrooms == updatedProperty.numberOfBathrooms &&
          retrievedProperty.incomeGenerated == updatedProperty.incomeGenerated
        ) {
          return res.status(200).send({ message: `The property with AirBnB ID ${req.body.airbnbId} does not contain any changes.` });
        } else {
          return Property
            .create({
              owner: req.body.owner,
              airbnbId: req.body.airbnbId,
              numberOfBedrooms: req.body.numberOfBedrooms,
              numberOfBathrooms: req.body.numberOfBathrooms,
              incomeGenerated: req.body.incomeGenerated,
            })
            .then(property => {
              Address
                .create({
                  line1: req.body.address.line1,
                  line2: req.body.address.line2,
                  line3: req.body.address.line3,
                  line4: req.body.address.line4,
                  postCode: req.body.address.postCode,
                  city: req.body.address.city,
                  country: req.body.address.country,
                  propertyId: property.id,
                })
                .then(() => {
                  return res.status(200).send({ message: `The property with AirBnB ID ${req.body.airbnbId} was successfully updated` })
                })
            })
        }
      })
      .catch((error) => {
        return res.status(400).send(error);
      });
  },

  remove(req, res) {
    return Property
      .findOne({
        where: {
          airbnbId: req.params.airbnbId
        },
      })
      .then(property => {
        if (!property) {
          return res.status(200).send({
            message: 'Property Not Found',
          });
        }
        Property
          .destroy({
            where: {
              airbnbId: req.params.airbnbId
            }
          })
          .then(() => {
            return res.status(204).send();
          });
      })
      .catch(error => {
        return res.status(400).send(error);
      });
  },
};
