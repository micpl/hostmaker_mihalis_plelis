'use strict';
module.exports = (sequelize, DataTypes) => {
  var Property = sequelize.define('Property', {
    owner: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    airbnbId: {
      type: DataTypes.BIGINT,
      allowNull: false,
    },
    numberOfBedrooms: {
      type: DataTypes.SMALLINT,
      allowNull: false,
      validate: { min: 0 },
    },
    numberOfBathrooms: {
      type: DataTypes.SMALLINT,
      allowNull: false,
      validate: { min: 1 },
    },
    incomeGenerated: {
      type: DataTypes.DECIMAL(15, 2),
      allowNull: false,
      validate: { min: 0.01 },
    },
  }, {
    timestamps: true,
    updatedAt: false,
  });

  Property.associate = (models) => {
    Property.hasOne(models.Address, {
      foreignKey: 'propertyId',
      as: 'address',
    });
  };
  
  return Property;
};