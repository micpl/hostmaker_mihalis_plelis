'use strict';
module.exports = (sequelize, DataTypes) => {
  var Address = sequelize.define('Address', {
    line1: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    line2: {
      type: DataTypes.TEXT,
      allowNull: true,
      defaultValue: null,
    },
    line3: {
      type: DataTypes.TEXT,
      allowNull: true,
      defaultValue: null,
    },
    line4: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    postCode: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    city: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    country: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    propertyId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      unique: true,
    },
  }, {
    timestamps: false,
  });

  Address.associate = (models) => {
    Address.belongsTo(models.Property, {
      foreignKey: 'propertyId',
      onDelete: 'CASCADE',
    });
  };

  return Address;
};